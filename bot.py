import discord
import config

client = discord.Client()

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith("!help"):
        msg = f"Hello {message.author.mention}"
        await message.channel.send(msg)


@client.event
async def on_ready():
    print("Bot status: working")
    print("Bot name:", client.user.name)
    print("Bot id:", client.user.id)
    print("Bot is on", len(client.guilds), "servers")

config.load()
client.run(config.data["token"])
